from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'story3.html')

def biodata(request):
    return render(request, 'story3bio.html')

def interest(request):
    return render(request, 'story3interest.html')

def education(request):
    return render(request, 'story3edu.html')

def experiences(request):
    return render(request, 'story3experiences.html')



